/*
 * Copyright Txus Ballesteros 2016 (@txusballesteros)
 *
 * This file is part of some open source application.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Contact: Txus Ballesteros <txus.ballesteros@gmail.com>
 */
package com.redbooth.demo;

import com.redbooth.WelcomeCoordinatorLayout;
import com.redbooth.WelcomePageBehavior;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

public class AnimationFlightTo extends WelcomePageBehavior {
    public static final long DURATION = 10000L;
    public static final int INIT_TIME = 1;
    public static final int FINAL_TIME = 2;
    public static final int Y = 1;
    public static final int X = 0;
    public static final int LENGTH_LOCATION_ARRAY = 2;
    private AnimatorProperty alphaAnimator;
    private AnimatorProperty objectAnimatorY;
    private AnimatorProperty objectAnimatorX;
    private AnimatorProperty objectAnimatorScaleX;
    private AnimatorProperty objectAnimatorScaleY;

    @Override
    protected void onCreate(WelcomeCoordinatorLayout coordinator) {
        System.out.println("AnimationFlightTo AnimationFlightTo onCreate");

    }

    private void configureTranslation() {
        final Component targetView = getTargetView();
        final Component shadowView = getTargetView().findComponentById(ResourceTable.Id_star_shadow);
        int[] viewLocation = new int[LENGTH_LOCATION_ARRAY];
        getLeftPositionFrom(targetView, viewLocation);
        int[] destinyViewLocation = new int[LENGTH_LOCATION_ARRAY];
        getLeftPositionFrom(getDestinyView(), destinyViewLocation);
        objectAnimatorY = new AnimatorProperty();
        objectAnimatorY.setDuration(DURATION);
        objectAnimatorY.moveFromY(0).moveToY( -(viewLocation[Y] - destinyViewLocation[Y]));
        objectAnimatorY.setCurveType(Animator.CurveType.ACCELERATE);
        objectAnimatorY.setTarget(targetView);


        objectAnimatorX = new AnimatorProperty();
        objectAnimatorX.setDuration(DURATION);
        objectAnimatorX.moveFromX(0).moveToX( -(viewLocation[X] - destinyViewLocation[X]));
        objectAnimatorX.setCurveType(Animator.CurveType.LINEAR);
        objectAnimatorX.setTarget(targetView);

        alphaAnimator = new AnimatorProperty();
        alphaAnimator.setTarget(shadowView);
        alphaAnimator.alphaFrom(0).alpha(0.4f).setCurveType(Animator.CurveType.LINEAR).setDuration(DURATION);
    }

    private void configureScale() {
        Component targetView = getTargetView();
        float scaleXFactor = ((float) getDestinyView().getEstimatedWidth() / (float) targetView.getEstimatedWidth());
        objectAnimatorScaleX = new AnimatorProperty();
        objectAnimatorScaleX.setDuration(DURATION).setTarget(targetView).setCurveType(Animator.CurveType.LINEAR);
        objectAnimatorScaleX.scaleX(scaleXFactor);


        float scaleYFactor = ((float) getDestinyView().getEstimatedHeight() / (float) targetView.getEstimatedHeight());
        objectAnimatorY = new AnimatorProperty();
        objectAnimatorScaleY.setDuration(DURATION).setTarget(targetView).setCurveType(Animator.CurveType.LINEAR);
        objectAnimatorScaleY.scaleY(scaleYFactor);
    }

    private void getLeftPositionFrom(Component view, int[] location) {
        int x = view.getLeft();
        int y = view.getTop();
        Component parent = (Component) view.getComponentParent();
        while(parent != null
                && !(parent instanceof WelcomeCoordinatorLayout)){
            x += parent.getLeft();
            y += parent.getTop();
            parent = (Component)parent.getComponentParent();
        }
        location[X] = x;
        location[Y] = y;
    }

    @Override
    protected void onPlaytimeChange(WelcomeCoordinatorLayout coordinator,
                                    float newPlaytime,
                                    float newScrollPosition) {
        if (newPlaytime <= INIT_TIME) {
            setCurrentTimeInAllAnimators(0);
        } else if (newPlaytime > INIT_TIME
                && newPlaytime <= FINAL_TIME) {
            long playTime = (long) ((newPlaytime - INIT_TIME) * DURATION);
            setCurrentTimeInAllAnimators(playTime);
        } else {
            setCurrentTimeInAllAnimators(DURATION);
        }
    }

    private void setCurrentTimeInAllAnimators(long playTime) {
//        objectAnimatorY.setCurrentPlayTime(playTime);
//        objectAnimatorX.setCurrentPlayTime(playTime);
//        objectAnimatorScaleX.setCurrentPlayTime(playTime);
//        objectAnimatorScaleY.setCurrentPlayTime(playTime);
//        alphaAnimator.setCurrentPlayTime(playTime);
    }
}
