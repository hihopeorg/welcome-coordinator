/*
 * Copyright Txus Ballesteros 2016 (@txusballesteros)
 *
 * This file is part of some open source application.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Contact: Txus Ballesteros <txus.ballesteros@gmail.com>
 */
package com.redbooth.demo;


import com.redbooth.WelcomeCoordinatorLayout;
import com.redbooth.WelcomePageBehavior;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.StackLayout;


public class ParallaxTitleBehaviour extends WelcomePageBehavior {

    private final static int PARALLAX_FACTOR = 4;
    private AnimatorValue parallaxAnimator;

    @Override
    protected void onCreate(WelcomeCoordinatorLayout coordinator) {
        System.out.println("ParallaxTitleBehaviour ParallaxTitleBehaviour onCreate()");
        final StackLayout.LayoutConfig params
                = (StackLayout.LayoutConfig) getPage().getLayoutConfig();
        long startDelay;
        long duration;
        float rightTranslation;
        float leftTranslation;
        if (params.getMarginLeft() == 0) {
            startDelay = 0;
            duration = getPage().getEstimatedWidth();
            rightTranslation = 0;
            leftTranslation = -(duration / PARALLAX_FACTOR);
        } else {
            startDelay = (params.getMarginLeft() - coordinator.getEstimatedWidth());
            duration = (getPage().getEstimatedWidth() * 2);
            rightTranslation = (duration / PARALLAX_FACTOR);
            leftTranslation = -(duration / PARALLAX_FACTOR);
        }

        parallaxAnimator = new AnimatorValue();
        parallaxAnimator.setCurveType(Animator.CurveType.LINEAR);
//        parallaxAnimator.setStartDelay(startDelay);
        parallaxAnimator.setDuration(duration);
        parallaxAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                float fraction = AnimatorValueUtil.getAnimatedValue(v, rightTranslation, leftTranslation);
                getTargetView().setTranslationX(fraction);
            }
        });

    }

    @Override
    protected void onPlaytimeChange(WelcomeCoordinatorLayout coordinator,
                                    float newPlaytime,
                                    float newScrollPosition) {
//        long currentPlaytime = (long)newScrollPosition;
//        if (newScrollPosition >= parallaxAnimator.getStartDelay()) {
//            currentPlaytime = (long)(newScrollPosition - parallaxAnimator.getStartDelay());
//        }
//        parallaxAnimator.setCurrentPlayTime(currentPlaytime);
    }
}
