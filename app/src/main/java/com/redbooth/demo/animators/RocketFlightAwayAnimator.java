/*
 * Copyright Txus Ballesteros 2016 (@txusballesteros)
 *
 * This file is part of some open source application.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Contact: Txus Ballesteros <txus.ballesteros@gmail.com>
 */
package com.redbooth.demo.animators;


import com.redbooth.demo.ResourceTable;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

public class RocketFlightAwayAnimator {
    private AnimatorGroup animator;
    private final Component rootView;

    public RocketFlightAwayAnimator(Component rootView) {
        this.rootView = rootView;
        initializeAnimator();
    }

    private void initializeAnimator() {
        final Component rocket = rootView.findComponentById(ResourceTable.Id_rocket_page4);
        Animator rocketScaleAnimator = getScaleAndVisibilityAnimator(rocket);
        Animator rocketRotationAnimator = getRotationAnimator(rocket);
        Animator rocketTranslationAnimator = getTranslationAnimator(rocket);
        animator = new AnimatorGroup();
        animator.setDelay(1000);
        animator.runParallel(rocketScaleAnimator, rocketRotationAnimator, rocketTranslationAnimator);
    }

    private AnimatorGroup getScaleAndVisibilityAnimator(final Component targetView) {
        AnimatorGroup animator = new AnimatorGroup();
        animator.setDuration(1000);
        AnimatorProperty scaleXAnimator = new AnimatorProperty();
        scaleXAnimator.setTarget(targetView);
        scaleXAnimator.scaleXFrom(1.0f).scaleX(0f);
        AnimatorProperty scaleYAnimator = new AnimatorProperty();
        scaleYAnimator.setTarget(targetView);
        scaleYAnimator.scaleYFrom(1.0f).scaleY(0f);
        animator.runParallel(scaleXAnimator, scaleYAnimator);
        animator.setStateChangedListener(new AnimatorListener() {
            @Override
            public void onStart(Animator animation) {
                targetView.setVisibility(Component.VISIBLE);
            }
        });
        return animator;
    }

    private AnimatorGroup getRotationAnimator(final Component targetView) {
        AnimatorGroup animator = new AnimatorGroup();
        animator.setDuration(1000);
        animator.setCurveType(Animator.CurveType.DECELERATE);
        AnimatorProperty scaleXAnimator= new AnimatorProperty();
        scaleXAnimator.setTarget(targetView);
        scaleXAnimator.rotate(45f);
        animator.runParallel(scaleXAnimator);
        return animator;
    }

    private AnimatorGroup getTranslationAnimator(final Component targetView) {
        AnimatorGroup animator = new AnimatorGroup();
        animator.setDuration(1000);
        AnimatorProperty translationXAnimator  = new AnimatorProperty();
        translationXAnimator.setTarget(targetView);
        translationXAnimator.moveFromX(-rootView.getWidth()/2.0f).moveToX(0f);
        AnimatorProperty translationYAnimator = new AnimatorProperty();
        translationYAnimator.setTarget(targetView);
        translationYAnimator.moveFromY(rootView.getWidth()/2.0f).moveToY(0f);
        translationYAnimator.setCurveType(Animator.CurveType.DECELERATE);
        animator.runParallel(translationXAnimator, translationYAnimator);
        return animator;
    }

    public void play() {
        animator.start();
    }

    public abstract class AnimatorListener implements Animator.StateChangedListener {
        public abstract void onStart(Animator var1);

        public void onStop(Animator var1){};

        public void onCancel(Animator var1){};

        public void onEnd(Animator var1){};

        public void onPause(Animator var1){};

        public void onResume(Animator var1){};
    }
}
