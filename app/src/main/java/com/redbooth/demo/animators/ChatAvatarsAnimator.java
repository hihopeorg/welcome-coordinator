/*
 * Copyright Txus Ballesteros 2016 (@txusballesteros)
 *
 * This file is part of some open source application.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Contact: Txus Ballesteros <txus.ballesteros@gmail.com>
 */
package com.redbooth.demo.animators;


import com.redbooth.demo.ResourceTable;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public class ChatAvatarsAnimator {
    private AnimatorGroup animator;
    private final Component rootView;

    public ChatAvatarsAnimator(Component rootView) {
        this.rootView = rootView;
        initializeAnimator();
    }

    private void initializeAnimator() {
        final Component avatar1 = rootView.findComponentById(ResourceTable.Id_avatar1_page2);
        final Component card1 = rootView.findComponentById(ResourceTable.Id_card1_page2);
        final Component avatar2 = rootView.findComponentById(ResourceTable.Id_avatar2_page2);
        final Component card2 = rootView.findComponentById(ResourceTable.Id_card2_page2);
        final Component star = rootView.findComponentById(ResourceTable.Id_star);
        AnimatorGroup avatar1Animator = getScaleAnimator(avatar1);
        AnimatorGroup card1Animator = getFlightFromLeft(card1);
        AnimatorGroup avatar2Animator = getScaleAnimator(avatar2);
        AnimatorGroup card2Animator = getFlightFromRight(card2);
        AnimatorGroup starAnimator = getScaleAndVisibilityAnimator(star);
        animator = new AnimatorGroup();
        animator.setDelay(500);
        animator.runSerially(avatar1Animator,card1Animator,avatar2Animator,card2Animator,starAnimator);
    }

    private AnimatorGroup getScaleAnimator(final Component targetView) {
        AnimatorGroup  animator= new AnimatorGroup();
        animator.setDuration(300);
        animator.setCurveType(Animator.CurveType.OVERSHOOT);
        AnimatorProperty scaleXAnimator = new AnimatorProperty();
        scaleXAnimator.setTarget(targetView);
        scaleXAnimator.scaleXFrom(0).scaleX(1.0f);
        AnimatorProperty scaleYAnimator = new AnimatorProperty();
        scaleYAnimator.setTarget(targetView);
        scaleYAnimator.scaleYFrom(0).scaleY(1.0f);
        animator.runParallel(scaleXAnimator,scaleYAnimator);
        return animator;
    }

    private AnimatorGroup getScaleAndVisibilityAnimator(final Component targetView) {
        AnimatorGroup  animator= new AnimatorGroup();
        animator.setDuration(300);
        animator.setCurveType(Animator.CurveType.OVERSHOOT);
        AnimatorProperty scaleXAnimator = new AnimatorProperty();
        scaleXAnimator.setTarget(targetView);
        scaleXAnimator.scaleXFrom(0).scaleX(1.0f);
        AnimatorProperty scaleYAnimator = new AnimatorProperty();
        scaleYAnimator.setTarget(targetView);
        scaleYAnimator.scaleYFrom(0).scaleY(1.0f);
        animator.runParallel(scaleXAnimator,scaleYAnimator);
        animator.setStateChangedListener(new AnimatorListener() {
            @Override
            public void onStart(Animator var1) {
                targetView.setVisibility(Component.VISIBLE);
            }
        });
        return animator;
    }

    private AnimatorGroup getFlightFromRight(final Component targetView) {
        AnimatorGroup animator = new AnimatorGroup();
        animator.setDuration(300);
        AnimatorProperty translationXAnimator= new AnimatorProperty();
        translationXAnimator.setTarget(targetView);
        translationXAnimator.moveFromX(targetView.getTranslationX()).moveToX(180);
        animator.setStateChangedListener(new AnimatorListener() {
            @Override
            public void onStart(Animator var1) {
                targetView.setVisibility(Component.VISIBLE);
            }
        });
        animator.runParallel(translationXAnimator);
        return animator;
    }

    private AnimatorGroup getFlightFromLeft(final Component targetView) {
        AnimatorGroup animator = new AnimatorGroup();
        animator.setDuration(600);
        AnimatorProperty translationXAnimator= new AnimatorProperty();
        translationXAnimator.setTarget(targetView);
        translationXAnimator.moveFromX(targetView.getTranslationX()).moveToX(500f);
        animator.setStateChangedListener(new AnimatorListener() {
            @Override
            public void onStart(Animator var1) {
                targetView.setVisibility(Component.VISIBLE);
            }
        });
        animator.runSerially(translationXAnimator);
        return animator;
    }

    public void play() {
        animator.start();
    }

    public abstract class AnimatorListener implements Animator.StateChangedListener {

        public abstract void onStart(Animator var1);

        public void onStop(Animator var1){};

        public void onCancel(Animator var1){};

        public void onEnd(Animator var1){};

        public  void onPause(Animator var1){};

        public  void onResume(Animator var1){};
    }

}
