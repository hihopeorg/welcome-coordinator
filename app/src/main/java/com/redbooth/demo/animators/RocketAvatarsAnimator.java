/*
 * Copyright Txus Ballesteros 2016 (@txusballesteros)
 *
 * This file is part of some open source application.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Contact: Txus Ballesteros <txus.ballesteros@gmail.com>
 */
package com.redbooth.demo.animators;

import com.redbooth.demo.ResourceTable;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.Image;

public class RocketAvatarsAnimator {
    private AnimatorGroup animator;
    private AnimatorGroup rocketFlameAnimator;
    private final Component rootView;

    public RocketAvatarsAnimator(Component rootView) {
        this.rootView = rootView;
        initializeAnimator();
    }

    private void initializeAnimator() {
        final Component rocketFlame = rootView.findComponentById(ResourceTable.Id_rocket_flame);
        final Component avatar1 = rootView.findComponentById(ResourceTable.Id_avatar1);
        final Component avatar2 = rootView.findComponentById(ResourceTable.Id_avatar2);
        final Component avatar3 = rootView.findComponentById(ResourceTable.Id_avatar3);
        final Component avatar4 = rootView.findComponentById(ResourceTable.Id_avatar4);


        Animator avatar1Animator = getAnimator(avatar1);
        Animator avatar2Animator = getAnimator(avatar2);
        Animator avatar3Animator = getAnimator(avatar3);
        Animator avatar4Animator = getAnimator(avatar4);
        Animator flameAnimator = getFlameAnimator(rocketFlame);
        animator = new AnimatorGroup();
        animator.setDelay(500);
        animator.runSerially(avatar4Animator,avatar3Animator,avatar2Animator,avatar1Animator);
        rocketFlameAnimator = new AnimatorGroup();
        rocketFlameAnimator.setDelay(500);
        rocketFlameAnimator.runSerially(flameAnimator);

    }

    private Animator getAnimator(Component targetView) {
        AnimatorGroup animator = new AnimatorGroup();
        animator.setDuration(300);
        animator.setCurveType(Animator.CurveType.OVERSHOOT);
        AnimatorProperty scaleXAnimator = new AnimatorProperty();
        scaleXAnimator.setTarget(targetView);
        scaleXAnimator.scaleXFrom(targetView.getScaleX()).scaleX(1.0f);
        AnimatorProperty scaleYAnimator = new AnimatorProperty();
        scaleYAnimator.setTarget(targetView);
        scaleYAnimator.scaleYFrom(targetView.getScaleY()).scaleY(1.0f);
        animator.runParallel(scaleXAnimator,scaleYAnimator);
        return animator;
    }

    private Animator getFlameAnimator(Component targetView) {
        AnimatorGroup animator = new AnimatorGroup();
        animator.setDuration(1000);
        animator.setCurveType(Animator.CurveType.LINEAR);
        AnimatorProperty alphaAnimator = new AnimatorProperty();
        alphaAnimator.setTarget(targetView);
        alphaAnimator.alphaFrom(0.8f).alpha(1f);
        AnimatorProperty scaleAnimator = new AnimatorProperty();
        scaleAnimator.setTarget(targetView);
        scaleAnimator.scaleYFrom(0.7f).scaleY(1f);
        alphaAnimator.setLoopedCount(Animator.INFINITE);
        scaleAnimator.setLoopedCount(Animator.INFINITE);
        animator.runParallel(alphaAnimator, scaleAnimator);
        return animator;
    }

    public void play() {
        animator.start();
        rocketFlameAnimator.start();
    }
}
