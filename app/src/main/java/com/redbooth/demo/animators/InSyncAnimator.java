/*
 * Copyright Txus Ballesteros 2016 (@txusballesteros)
 *
 * This file is part of some open source application.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Contact: Txus Ballesteros <txus.ballesteros@gmail.com>
 */
package com.redbooth.demo.animators;


import com.redbooth.demo.ResourceTable;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

public class InSyncAnimator {
    private AnimatorGroup animator;
    private final Component rootView;

    public InSyncAnimator(Component rootView) {
        this.rootView = rootView;
        initializeAnimator();
    }

    private void initializeAnimator() {
        final Component avatarView = rootView.findComponentById(ResourceTable.Id_avatar5);
        final Component arrowChartMaskView = rootView.findComponentById(ResourceTable.Id_arrow_chart_mask);
        final AnimatorProperty scaleXAnimator= new AnimatorProperty();
        scaleXAnimator.setTarget(avatarView);
        scaleXAnimator.setDuration(300);
        scaleXAnimator.setCurveType(Animator.CurveType.OVERSHOOT);
        scaleXAnimator.scaleXFrom(avatarView.getScaleX()).scaleX(1);
        scaleXAnimator.scaleYFrom(avatarView.getScaleY()).scaleY(1);

        final AnimatorProperty maskScaleXAnimator = new AnimatorProperty();
        maskScaleXAnimator.setTarget(arrowChartMaskView);
        maskScaleXAnimator.setDuration(300);
        maskScaleXAnimator.setCurveType(Animator.CurveType.LINEAR);
        maskScaleXAnimator.scaleXFrom(1.0f).scaleY(0.0f);
        animator = new AnimatorGroup();
        AnimatorGroup together = new AnimatorGroup();
        together.runParallel(scaleXAnimator);
        AnimatorGroup mask= new AnimatorGroup();
        mask.runParallel(maskScaleXAnimator);
        animator.setDelay(500);
        animator.runSerially(together,mask);
        avatarView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                animator.start();
            }
        });
    }

    public void play() {
        animator.start();
    }
}
