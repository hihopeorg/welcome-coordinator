/*
 * Copyright Txus Ballesteros 2016 (@txusballesteros)
 *
 * This file is part of some open source application.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Contact: Txus Ballesteros <txus.ballesteros@gmail.com>
 */
package com.redbooth.demo;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class RectangleWithCapCircleView extends Component implements Component.EstimateSizeListener , Component.DrawTask {
    private Paint paint;
    private RectFloat rectCap;
    private RectFloat rectBottom;
    private int arc =0;

    public RectangleWithCapCircleView(Context context) {
        super(context);
        init();
    }

    public RectangleWithCapCircleView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public RectangleWithCapCircleView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init(){
        setEstimateSizeListener(this::onEstimateSize);
        addDrawTask(this::onDraw);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        rectCap = null;
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (rectCap == null) {
            rectCap = new RectFloat(0f, 0f, getWidth(), getWidth() + 1);
            rectBottom = new RectFloat(0.0f, (getWidth()/2) - 1, getWidth(), getHeight());
            paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(Color.WHITE);
        }
        canvas.drawRect(rectBottom, paint);
        canvas.drawArc(rectCap, new Arc(0f,-180f,true), paint);
    }
}

