package com.redbooth.demo;

import com.redbooth.WelcomeCoordinatorLayout;
import com.redbooth.demo.animators.ChatAvatarsAnimator;
import com.redbooth.demo.animators.InSyncAnimator;
import com.redbooth.demo.animators.RocketAvatarsAnimator;
import com.redbooth.demo.animators.RocketFlightAwayAnimator;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class MainActivity extends Ability {
    private boolean animationReady = false;
    private AnimatorValue backgroundAnimator;
    WelcomeCoordinatorLayout coordinatorLayout;
    private RocketAvatarsAnimator rocketAvatarsAnimator;
    private ChatAvatarsAnimator chatAvatarsAnimator;
    private RocketFlightAwayAnimator rocketFlightAwayAnimator;
    private InSyncAnimator inSyncAnimator;
    Button skip;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_main);
        coordinatorLayout = (WelcomeCoordinatorLayout) findComponentById(ResourceTable.Id_coordinator);
        initializeListeners();
        initializePages();
        initializeBackgroundTransitions();
        skip = (Button) findComponentById(ResourceTable.Id_skip);
        skip.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                skip();
            }
        });
    }


    @Override
    protected void onActive() {
        super.onActive();
    }

    private void initializePages() {
        coordinatorLayout.addPage(ResourceTable.Layout_welcome_page_1,
                ResourceTable.Layout_welcome_page_2,
                ResourceTable.Layout_welcome_page_3,
                ResourceTable.Layout_welcome_page_4);
    }

    private void initializeListeners() {
        coordinatorLayout.setOnPageScrollListener(new WelcomeCoordinatorLayout.OnPageScrollListener() {
            @Override
            public void onScrollPage(Component v, float progress, float maximum) {

            }

            @Override
            public void onPageSelected(Component v, int pageSelected) {
                switch (pageSelected) {
                    case 0:
                        if (rocketAvatarsAnimator == null) {
                            rocketAvatarsAnimator = new RocketAvatarsAnimator(coordinatorLayout);
                            rocketAvatarsAnimator.play();
                        }
                        break;
                    case 1:
                        if (chatAvatarsAnimator == null) {
                            chatAvatarsAnimator = new ChatAvatarsAnimator(coordinatorLayout);
                            chatAvatarsAnimator.play();
                        }
                        break;
                    case 2:
                        if (inSyncAnimator == null) {
                            inSyncAnimator = new InSyncAnimator(coordinatorLayout);
                            inSyncAnimator.play();
                        }

                        break;
                    case 3:
                        if (rocketFlightAwayAnimator == null) {
                            rocketFlightAwayAnimator = new RocketFlightAwayAnimator(coordinatorLayout);
                            rocketFlightAwayAnimator.play();
                        }
                        break;
                }
            }
        });
    }

    private void initializeBackgroundTransitions() {
        final ResourceManager resources = getResourceManager();
        final int colorPage1;
        final int colorPage2;
        final int colorPage3;
        final int colorPage4;
        try {
            colorPage1 = resources.getElement(ResourceTable.Color_page1).getColor();
           colorPage2 = resources.getElement(ResourceTable.Color_page2).getColor();
           colorPage3 = resources.getElement(ResourceTable.Color_page3).getColor();
           colorPage4 = resources.getElement(ResourceTable.Color_page4).getColor();
           backgroundAnimator = new AnimatorValue();
           ShapeElement shapeElement = new ShapeElement();
           backgroundAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
               @Override
               public void onUpdate(AnimatorValue animatorValue, float v) {
                   float fraction = AnimatorValueUtil.getAnimatedValue(v,colorPage1,colorPage2,colorPage3,colorPage4);
                   shapeElement.setRgbColor(RgbColor.fromArgbInt((int) fraction));
                   coordinatorLayout.setBackground(shapeElement);
               }
           });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }

    }
    void skip() {
        coordinatorLayout.setCurrentPage(coordinatorLayout.getNumOfPages() - 1, true);
    }
}
