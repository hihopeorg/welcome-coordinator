# welcome-coordinator

**本项目是基于开源项目welcome-coordinator进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/txusballesteros/welcome-coordinator ）追踪到原项目版本**

#### 项目介绍

- 项目名称：welcome-coordinator
- 所属系列：ohos的第三方组件适配移植
- 功能： 自定义动态欢迎引导页面
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/txusballesteros/welcome-coordinator
- 原项目基线版本：1.0.1，sha1:4363cd293d6f2d793c30e1a92870d7e65694a36b
- 编程语言：Java
- 外部库依赖：无

#### 效果演示

<img src="screenshot/operation.gif"/>

#### 安装教程

方案一：

1. 编译har包WelcomeCoordinator.har。
2. 启动 DevEco Studio，将har包导入工程目录“app->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'WelcomeCoordinator', ext: 'har')
	……
}
```

方案二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.redbooth.ohos:WelcomeCoordinator:1.0.0'
}
```
#### 使用说明

##### 1.- 添加自定义视图

```xml
<com.redbooth.WelcomeCoordinatorLayout
     ohos:id="$+id:coordinator"
     ohos:height="match_parent"
     ohos:width="match_parent" />
```

##### 2.- 页面建模

```xml
<com.redbooth.WelcomePageLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos">
    
    ...
    
</com.redbooth.WelcomePageLayout>
```

**警告** 不要忘记创建WelcomePageLayout作为页面的根元素.

##### 3.- 将页面添加到协调器

```java
@Override
protected void onCreate(Bundle savedInstanceState) {
    ...
    final WelcomeCoordinatorLayout coordinatorLayout 
            = (WelcomeCoordinatorLayout)findViewById(R.id.coordinator); 
    coordinatorLayout.addPage(R.layout.welcome_page_1,
                              ...,
                              R.layout.welcome_page_4);
}
```

##### 4.- 创建自己的行为

```java
public class ParallaxTitleBehaviour extends WelcomePageBehavior {
    @Override
    protected void onCreate(WelcomeCoordinatorLayout coordinator) {
        ...
    }
    
    @Override
    protected void onPlaytimeChange(WelcomeCoordinatorLayout coordinator,
                                    float newPlaytime,
                                    float newScrollPosition) {
        ...
    }
}
```

##### 5.- 设置自己的行为属性

```xml

<com.redbooth.WelcomePageLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        xmlns:app="http://schemas.huawei.com/apk/res/ohos">
    
    <Text
        ...
        app:view_behavior=".ParallaxTitleBehaviour" />
            
</com.redbooth.WelcomePageLayout>
```

#### 版本迭代

- v1.0.0

#### 版权和许可信息
```
Copyright Txus Ballesteros & Francisco Sirvent 2016

This file is part of some open source application.

Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
```