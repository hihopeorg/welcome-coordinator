/*
 * Copyright Txus Ballesteros 2016 (@txusballesteros)
 *
 * This file is part of some open source application.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Contact: Txus Ballesteros <txus.ballesteros@gmail.com>
 */
package com.redbooth;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.utils.TextTool;
import ohos.app.Context;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class WelcomePageLayout extends DependentLayout {

    public WelcomePageLayout(Context context) {
        super(context);
    }

    public WelcomePageLayout(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public WelcomePageLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public ComponentContainer.LayoutConfig createLayoutConfig(Context context, AttrSet attrSet) {
        return new LayoutParams(context, attrSet);
    }


    List<WelcomePageBehavior> getBehaviors(WelcomeCoordinatorLayout coordinatorLayout) {
        List<WelcomePageBehavior> result = new ArrayList<>();

        for (int index = 0; index < getChildCount(); index++) {
            final Component view = getComponentAt(index);
            if (view.getLayoutConfig() instanceof LayoutParams) {
                final LayoutParams params = (LayoutParams) view.getLayoutConfig();
                final WelcomePageBehavior behavior = params.getBehavior();
                if (behavior != null) {
                    behavior.setCoordinator(coordinatorLayout);
                    behavior.setTarget(view);
                    behavior.setPage(this);
                    result.add(behavior);
                }
            }
        }
        return result;
    }

    public static class LayoutParams extends DependentLayout.LayoutConfig {
        public final static int NO_DESTINY_VIEW = -1;
        private int destinyViewId = NO_DESTINY_VIEW;
        private WelcomePageBehavior behavior;

        public WelcomePageBehavior getBehavior() {
            return behavior;
        }

        public int getDestinyViewId() {
            return destinyViewId;
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(DependentLayout.LayoutConfig source) {
            super(source);
        }

        public LayoutParams(ComponentContainer.LayoutConfig source) {
            super(source);
        }


        public LayoutParams(Context context, AttrSet attrs) {
            super(context, attrs);
            extractAttributes(context, attrs);
        }

        private void extractAttributes(Context context, AttrSet attrs) {
            behavior = parseBehavior(context, AttrSetUtil.getString(attrs, AttrConstant.VIEW_BEHAVIOR, ""));
            destinyViewId = AttrSetUtil.getInt(attrs, AttrConstant.DESTINY, NO_DESTINY_VIEW);
        }

        private WelcomePageBehavior parseBehavior(Context context, String name) {
            WelcomePageBehavior result = null;
            if (!TextTool.isNullOrEmpty(name)) {
                final String fullName;
                if (name.startsWith(".")) {
                    fullName = context.getBundleName() + name;
                } else {
                    fullName = name;
                }
                try {
                    Class<WelcomePageBehavior> behaviorClazz
                            = (Class<WelcomePageBehavior>) Class.forName(fullName);
                    final Constructor<WelcomePageBehavior> mainConstructor
                            = behaviorClazz.getConstructor();
                    mainConstructor.setAccessible(true);
                    result = mainConstructor.newInstance();
                } catch (Exception e) {
                    throw new RuntimeException("Could not inflate Behavior subclass " + fullName, e);
                }
            }
            return result;
        }
    }
}
