/*
 * Copyright Txus Ballesteros 2016 (@txusballesteros)
 *
 * This file is part of some open source application.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Contact: Txus Ballesteros <txus.ballesteros@gmail.com>
 */
package com.redbooth;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.components.DragInfo;
import ohos.agp.components.VelocityDetector;
import ohos.agp.window.service.DisplayManager;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

class WelcomeCoordinatorTouchController {
    public static final int MAX_VELOCITY = 300;
    public static final int CURRENT_VELOCITY = 1000;
    public static final long SMOOTH_SCROLL_DURATION = 350;
    public static final String PROPERTY_SCROLL_X = "scrollX";
    private final WelcomeCoordinatorLayout view;
    private float iniEventX;
    private int currentScrollX;
    private int minScroll = 0;
    private int maxScroll = 0;
    private int iniEventY;
    private VelocityDetector velocityTracker;
    private AnimatorValue smoothScrollAnimator;
    private OnPageScrollListener onPageScrollListener;

    public WelcomeCoordinatorTouchController(final WelcomeCoordinatorLayout view) {
        this.view = view;
        view.setDraggedListener(Component.DRAG_DOWN, new DraggedListenerIml());
        configureLayoutListener();
    }

    private void configureLayoutListener() {
        view.getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                WelcomeCoordinatorLayout welcomeCoordinatorLayout
                        = WelcomeCoordinatorTouchController.this.view;
                int pagesSteps = welcomeCoordinatorLayout.getNumOfPages() - 1;
                maxScroll = pagesSteps * DisplayManager.getInstance().getDefaultDisplay(view.getContext()).get().getAttributes().width;
                configureScrollListener();
            }
        }, 100);


    }

    private void configureScrollListener() {
        view.addScrolledListener(new Component.ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
                view.notifyProgressScroll(view.getScrollValue(Component.AXIS_X) / (float) view.getWidth(),
                        view.getScrollValue(Component.AXIS_X));
            }
        });
    }

    protected boolean onTouchEvent(TouchEvent event) {
        MmiPoint point = event.getPointerPosition(0);
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                if (velocityTracker == null) {
                    velocityTracker = VelocityDetector.obtainInstance();
                } else {
                    velocityTracker.clear();
                }
                velocityTracker.addEvent(event);
                currentScrollX = view.getScrollValue(Component.AXIS_X);
                iniEventX = point.getX();
                break;
            case TouchEvent.POINT_MOVE:
                setScrollX(scrollLimited(point.getX()));
                velocityTracker.addEvent(event);
                velocityTracker.calculateCurrentVelocity(CURRENT_VELOCITY);
                break;
            case TouchEvent.PRIMARY_POINT_UP:
            case TouchEvent.CANCEL:
                setScrollX(scrollLimited(point.getX()));
                velocityTracker.addEvent(event);
                velocityTracker.calculateCurrentVelocity(CURRENT_VELOCITY);
                evaluateMoveToPage();
                break;
        }
        return true;
    }

    private void evaluateMoveToPage() {
        float xVelocity = velocityTracker.getHorizontalVelocity();
        currentScrollX = view.getScrollValue(Component.AXIS_X);
        moveToPagePosition(xVelocity);
    }

    private int scrollLimited(float scrollX) {
        int newScrollPosition = (int) (currentScrollX + iniEventX - scrollX);
        return Math.max(minScroll, Math.min(newScrollPosition, maxScroll));
    }

    private void setScrollX(int value) {
        view.scrollTo(value, 0);
        if (onPageScrollListener != null) {
            onPageScrollListener.onScrollPage(value);
        }
    }

    private void moveToPagePosition(float xVelocity) {
        float width = view.getWidth();
        int currentPage = (int) Math.floor(currentScrollX / width);
        float percentageOfNewPageVisible = (currentScrollX % width) / width;
        if (xVelocity < -MAX_VELOCITY) {
            scrollToPage(currentPage + 1);
        } else if (xVelocity > MAX_VELOCITY) {
            scrollToPage(currentPage);
        } else if (percentageOfNewPageVisible > 0.5f) {
            scrollToPage(currentPage + 1);
        } else {
            scrollToPage(currentPage);
        }
    }

    public void scrollToPage(int newCurrentPage) {
        scrollToPage(newCurrentPage, true);
    }

    public void scrollToPage(int newCurrentPage, boolean animated) {
        int width = view.getWidth();
        int limitedNumPage = Math.max(0, Math.min(view.getNumOfPages() - 1, newCurrentPage));
        int scrollX = limitedNumPage * width;
        if (animated) {
            smoothScrollX(scrollX, newCurrentPage);
        } else {
            setScrollX(scrollX);
        }
    }

    private void smoothScrollX(int scrollX, int newCurrentPage) {
        cancelScrollAnimationIfNecessary();
        if (view.getScrollValue(Component.AXIS_X) != scrollX) {
            smoothScrollAnimator = new AnimatorValue();
            smoothScrollAnimator.setDuration(SMOOTH_SCROLL_DURATION);
            smoothScrollAnimator.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    if (onPageScrollListener != null) {
                        int width = view.getWidth();
                        int page = view.getScrollValue(Component.AXIS_X) / width;
                        int limitedNumPage = Math.max(0, Math.min(view.getNumOfPages() - 1, page));
                        onPageScrollListener.onPageSelected(limitedNumPage);

                    }
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            smoothScrollAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    float faction = getAnimatedValue(v, view.getScrollValue(Component.AXIS_X), scrollX);
                    view.scrollTo((int) faction, 0);
                    if (onPageScrollListener != null) {
                        onPageScrollListener.onScrollPage(faction);
                    }
                }
            });
            smoothScrollAnimator.start();
        }
    }

    private float getAnimatedValue(float fraction, float... values) {
        if (values == null || values.length == 0) {
            return 0;
        }
        if (values.length == 1) {
            return values[0] * fraction;
        } else {
            if (fraction == 1) {
                return values[values.length - 1];
            }
            float oneFraction = 1f / (values.length - 1);
            float offFraction = 0;
            for (int i = 0; i < values.length - 1; i++) {
                if (offFraction + oneFraction >= fraction) {
                    return values[i] + (fraction - offFraction) * (values.length - 1) * (values[i + 1] - values[i]);
                }
                offFraction += oneFraction;
            }
        }
        return 0;
    }

    private void cancelScrollAnimationIfNecessary() {
        if ((smoothScrollAnimator != null)
                && (smoothScrollAnimator.isRunning())) {
            smoothScrollAnimator.cancel();
            smoothScrollAnimator = null;
        }
    }

    public void setOnPageScrollListener(OnPageScrollListener onPageScrollListener) {
        this.onPageScrollListener = onPageScrollListener;
    }

    public interface OnPageScrollListener {
        void onScrollPage(float progress);

        void onPageSelected(int pageSelected);
    }

    private class DraggedListenerIml implements Component.DraggedListener {

        @Override
        public void onDragDown(Component component, DragInfo dragInfo) {
        }

        @Override
        public void onDragStart(Component component, DragInfo dragInfo) {
        }

        @Override
        public void onDragUpdate(Component component, DragInfo dragInfo) {
        }

        @Override
        public void onDragEnd(Component component, DragInfo dragInfo) {
        }

        @Override
        public void onDragCancel(Component component, DragInfo dragInfo) {
        }

    }
}
