/*
 * Copyright Txus Ballesteros 2016 (@txusballesteros)
 *
 * This file is part of some open source application.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Contact: Txus Ballesteros <txus.ballesteros@gmail.com>
 */
package com.redbooth;


import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

public class WelcomeCoordinatorLayout extends ScrollView implements Component.DrawTask, Component.TouchEventListener {
    public static final boolean ANIMATED = true;
    public static final boolean INANIMATED = false;
    public static final int WITHOUT_MARGIN = 0;
    public static final int RADIUS = 12;
    public static final int RADIUS_MARGIN = 30;
    public static final Color DEF_INDICATOR_UNSELECTED_COLOR = Color.WHITE;
    public static final Color DEF_INDICATOR_SELECTED_COLOR = Color.BLACK;
    private WelcomeCoordinatorTouchController touchController;
    private WelcomeCoordinatorPageInflater pageInflater;
    private StackLayout mainContentView;
    private List<WelcomePageBehavior> behaviors = new ArrayList<>();
    private OnPageScrollListener onPageScrollListener;
    private int pageSelected = 0;
    private Color indicatorColorUnselected = DEF_INDICATOR_UNSELECTED_COLOR;
    private Color indicatorColorSelected = DEF_INDICATOR_SELECTED_COLOR;
    private Paint indicatorPaintUnselected;
    private Paint indicatorPaintSelected;
    private boolean showIndicators = true;
    private boolean scrollingEnabled = true;
    private int count = 0;

    public WelcomeCoordinatorLayout(Context context) {
        super(context);
        init();
    }

    public WelcomeCoordinatorLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        initializeView(attrs);
    }

    public WelcomeCoordinatorLayout(Context context, AttrSet attrs, String defStyleAttr) {

        super(context, attrs, defStyleAttr);
        initializeView(attrs);
    }

    @Override
    public boolean requestFocus() {
        return true;
    }


    public void showIndicators(boolean show) {
        this.showIndicators = show;
    }

    public void setScrollingEnabled(boolean enabled) {
        this.scrollingEnabled = enabled;
    }


    public void addPage(int... layoutResourceIds) {
        for (int i = layoutResourceIds.length - 1; i >= 0; i--) {
            int layoutResourceId = layoutResourceIds[i];
            final ComponentContainer pageView = (ComponentContainer) pageInflater.inflate(layoutResourceId);
            final List<WelcomePageBehavior> pageBehaviors = extractPageBehaviors(pageView);
            if (!pageBehaviors.isEmpty()) {
                this.behaviors.addAll(pageBehaviors);
            }
            mainContentView.addComponent(pageView);
        }
        if (onPageScrollListener != null) {
            onPageScrollListener.onPageSelected(this, 0);
        }
        getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
            @Override
            public void run() {
                postLayout();
            }
        });

    }

    private List<WelcomePageBehavior> extractPageBehaviors(Component view) {
        List<WelcomePageBehavior> behaviors = new ArrayList<>();
        if (view instanceof WelcomePageLayout) {
            final WelcomePageLayout pageLayout = (WelcomePageLayout) view;
            final List<WelcomePageBehavior> pageBehaviors = pageLayout.getBehaviors(this);
            if (!pageBehaviors.isEmpty()) {
                behaviors.addAll(pageBehaviors);
            }

        }
        return behaviors;
    }

    public int getNumOfPages() {
        int result = 0;
        if (mainContentView != null) {
            result = mainContentView.getChildCount();
        }
        return result;
    }

    private void init(){
        this.enableScrollBar(DirectionalLayout.HORIZONTAL, false);
        this.setScrollbarOverlapEnabled(false);
        touchController = new WelcomeCoordinatorTouchController(this);
        pageInflater = new WelcomeCoordinatorPageInflater(this);
        setTouchEventListener(this::onTouchEvent);
        addDrawTask(this::onDraw);
        buildMainContentView();
        attachMainContentView();
        configureIndicatorColors();
    }
    private void initializeView(AttrSet attrs) {
        this.enableScrollBar(DirectionalLayout.HORIZONTAL, false);
        this.setScrollbarOverlapEnabled(false);
        touchController = new WelcomeCoordinatorTouchController(this);
        pageInflater = new WelcomeCoordinatorPageInflater(this);
        setTouchEventListener(this::onTouchEvent);
        addDrawTask(this::onDraw);
        extractAttributes(attrs);
        buildMainContentView();
        attachMainContentView();
        configureIndicatorColors();
    }

    private void extractAttributes(AttrSet attrs) {
        indicatorColorUnselected = AttrSetUtil.getColor(attrs, AttrConstant.INDICATOR_UNSELECTED, DEF_INDICATOR_UNSELECTED_COLOR);
        indicatorColorSelected = AttrSetUtil.getColor(attrs, AttrConstant.INDICATOR_SELECTED, DEF_INDICATOR_UNSELECTED_COLOR);
        showIndicators = AttrSetUtil.getBoolean(attrs, AttrConstant.SHOW_INDICATORS, showIndicators);
        scrollingEnabled = AttrSetUtil.getBoolean(attrs, AttrConstant.SCROLLING_ENABLED, scrollingEnabled);
    }

    private void buildMainContentView() {
        mainContentView = new StackLayout(this.getContext());
        mainContentView.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT));
        mainContentView.setClipEnabled(false);
    }

    private void attachMainContentView() {
        removeAllComponents();
        addComponent(mainContentView);
    }

    private void configureIndicatorColors() {
        indicatorPaintUnselected = new Paint();
        indicatorPaintUnselected.setColor(indicatorColorUnselected);
        indicatorPaintSelected = new Paint();
        indicatorPaintSelected.setColor(indicatorColorSelected);
    }

    public void setIndicatorColorSelected(int indicatorColorSelected) {
        this.indicatorColorSelected = new Color(indicatorColorSelected);
        indicatorPaintSelected.setColor(new Color(indicatorColorSelected));
    }

    public void setIndicatorColorUnselected(int indicatorColorUnselected) {
        this.indicatorColorUnselected = new Color(indicatorColorUnselected);
        indicatorPaintUnselected.setColor(new Color(indicatorColorUnselected));
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        for (int index = 0; index < getNumOfPages(); index++) {
            ComponentContainer childAt = (ComponentContainer) mainContentView.getComponentAt(index);
            configurePageLayout(childAt, index);
        }
        if (showIndicators) {
            drawIndicator(canvas);
        }
    }


    private void configurePageLayout(ComponentContainer pageView, int position) {
        int coordinatorWidth = getEstimatedWidth();
        int reversePosition = getNumOfPages() - 1 - position;
        int pageMarginLeft = (coordinatorWidth * reversePosition);
        int originalHeight = pageView.getLayoutConfig().height;
        LayoutConfig layoutParams = new LayoutConfig(coordinatorWidth, originalHeight);
        layoutParams.setMargins(pageMarginLeft, WITHOUT_MARGIN, WITHOUT_MARGIN, WITHOUT_MARGIN);
        pageView.setLayoutConfig(layoutParams);
    }

    private void drawIndicator(Canvas canvas) {
        int centerX = (getWidth() - RADIUS) / 2 + RADIUS / 2;
        int indicatorWidth = RADIUS * 2;
        int indicatorAndMargin = indicatorWidth + RADIUS_MARGIN;
        int leftIndicators = centerX - ((getNumOfPages() - 1) * indicatorAndMargin) / 2;
        int positionY = getHeight() - RADIUS - RADIUS_MARGIN;
        for (int i = 0; i < getNumOfPages(); i++) {
            int x = leftIndicators + indicatorAndMargin * i + getScrollValue(Component.AXIS_X);
            canvas.drawCircle(x, positionY, RADIUS, indicatorPaintUnselected);
        }
        float width = (float) getWidth();
        float scrollProgress = getScrollValue(Component.AXIS_X) / width;
        float selectedXPosition = leftIndicators + getScrollValue(Component.AXIS_X) + scrollProgress * indicatorAndMargin;
        canvas.drawCircle(selectedXPosition, positionY, RADIUS, indicatorPaintSelected);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        boolean touchEventCaptured = false;
        if (scrollingEnabled) {
            touchEventCaptured = touchController.onTouchEvent(event);
        }
        return touchEventCaptured;
    }

    public void notifyProgressScroll(float progress, float scroll) {
        for (WelcomePageBehavior welcomePageBehavior : behaviors) {
            welcomePageBehavior.onPlaytimeChange(this, progress, scroll);
        }
    }

    public void setOnPageScrollListener(OnPageScrollListener onPageScrollListener) {
        this.onPageScrollListener = onPageScrollListener;
        touchController.setOnPageScrollListener(new WelcomeCoordinatorTouchController.OnPageScrollListener() {
            @Override
            public void onScrollPage(float progress) {
                int numOfPages = (getNumOfPages() - 1);
                int maximumScroll = getEstimatedWidth() * numOfPages;
                WelcomeCoordinatorLayout.this.onPageScrollListener.onScrollPage(WelcomeCoordinatorLayout.this, progress, maximumScroll);
            }

            @Override
            public void onPageSelected(int pageSelected) {
                WelcomeCoordinatorLayout.this.pageSelected = pageSelected;
                WelcomeCoordinatorLayout.this.onPageScrollListener.onPageSelected(WelcomeCoordinatorLayout.this, pageSelected);
            }
        });
    }

    public int getPageSelected() {
        return pageSelected;
    }

    public void setCurrentPage(int newCurrentPage, boolean animated) {
        pageSelected = Math.max(0, Math.min(getNumOfPages() - 1, newCurrentPage));
        touchController.scrollToPage(pageSelected, animated);
    }


    public interface OnPageScrollListener {
        void onScrollPage(Component v, float progress, float maximum);

        void onPageSelected(Component v, int pageSelected);
    }
}
